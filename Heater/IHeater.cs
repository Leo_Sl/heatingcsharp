﻿using RuntimeLib;

namespace HeaterN
{
    public interface IHeater : IEventHandler<HeaterOffEvent>, IEventHandler<HeaterOnEvent>
    {

    }
}
