﻿namespace HeaterN
{
    public interface IHeaterHAL
    {
        void SwitchOn();
        void SwitchOff();
    }
}
