﻿using System;

namespace HeaterN
{

    public class Heater : IHeater
    {
        private enum State
        {
            on,
            off
        }

        private State state = State.off;
        private readonly IHeaterHAL heater;
        public bool IsOn
        {
            get { return state == State.on; }
            set { state = value ? State.on : State.off; }
        }

        public Heater(IHeaterHAL heaterHAL)
        {
            heater = heaterHAL;
        }

        public void HandleEvent(HeaterOffEvent e)
        {
            switch (state)
            {
                case State.on:
                    heater.SwitchOff();
                    state = State.off;
                    break;
                case State.off:
                    break;
            }
        }
        
        public void HandleEvent(HeaterOnEvent e)
        {
            Console.WriteLine("HEATERON");
            switch (state)
            {
                case State.off:
                    heater.SwitchOn();
                    state = State.on;
                    break;
                case State.on:
                    break;
            }
        }
    }
}
