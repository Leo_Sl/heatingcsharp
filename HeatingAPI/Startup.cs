using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using HeatingSimulations;
using Sensor;
using HeaterN;
using RuntimeLib;
using System;
using Controller;
using System.Threading.Tasks;
using Microsoft.VisualStudio.Threading;
using HeatingAPI.Controllers;
using System.Collections.Generic;

namespace HeatingAPI
{
    internal interface IReader : IEventHandler<TemperatureEvent>
    {
        public Task<Temperature> readTempAsync();
    }
    internal class OneTimeTemperatureReader : IReader
    {
        // is this actually a transient service?
        private readonly EventQueue _queue;
        private readonly Dispatcher _dispatcher;
        private readonly AsyncQueue<Temperature> _asyncQueue = new AsyncQueue<Temperature>();

        public OneTimeTemperatureReader(Dispatcher dispatcher, EventQueue queue)
        {
            _queue = queue;
            _dispatcher = dispatcher;
            dispatcher.Subscribe(this);
        }

        public void HandleEvent(TemperatureEvent @event)
        {
            _asyncQueue.Enqueue(@event.Temperature);
            //_dispatcher.unsub();
        }

        public Task<Temperature> readTempAsync()
        {
            _queue.Enqueue(new ReadTemperatureEvent());
            // state, if read temp event from sth else and we already handled it
            return _asyncQueue.DequeueAsync();
        }
    }

    public interface ISimStorage
    {
        void addSim(int id, Simulation s);
        void removeSim(int s); // id?
        public bool Has(int id);
        Simulation Get(int id);
    }

    public class SimStorage : ISimStorage
    {
        public Dictionary<int, Simulation> _storage = new Dictionary<int, Simulation>();
        public void addSim(int id, Simulation s)
        {
            _storage.Add(id, s); // check empty?
        }

        public Simulation Get(int id)
        {
            if (!Has(id))
            {
                throw new ArgumentException("index does not exist");
            }
            return _storage[id];
        }

        public bool Has(int id)
        {
            return _storage.ContainsKey(id);
        }

        public void removeSim(int id)
        {
            throw new NotImplementedException();
        }
    }

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<ISimStorage, SimStorage>();
            //var sim = new HeatingSimulation(Temperature.FromCelsius(22));
            //services.AddSingleton<ISensorHAL>(sim);
            //services.AddSingleton<IHeaterHAL>(sim);
            //services.AddSingleton<HeatingSimulation>(sim);
            //var queue = new EventQueue(
            //    typeof(TemperatureEvent), typeof(TimerEvent), typeof(TemperatureControllerOffEvent), typeof(ReadTemperatureEvent)
                
            //    );
            
            //services.AddSingleton<EventQueue>(queue);
            //services.AddSingleton<ITemperatureControllerCallback>(queue);
            //services.AddSingleton<ITemperatureSensorCallback>(queue);
            //services.AddSingleton<Dispatcher>();
            //services.AddSingleton<Heater>();
            //services.AddSingleton<TemperatureController>();
            //services.AddSingleton<TemperatureSensor>();
            //services.AddSingleton<EventTimerService>();

            //services.AddTransient<IReader, OneTimeTemperatureReader>();

            services.Configure<IISServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

           

            //var dispatcherLoop = Task.Run(async () =>
            //{
            //    var queue = app.ApplicationServices.GetService<EventQueue>()!;
            //    var dispatcher = app.ApplicationServices.GetService<Dispatcher>()!;
            //    while (true)
            //    {
            //        var ev = await queue.DequeueAsync();
            //        dispatcher.Publish(ev);
            //    }
            //});

            //SetupDispatcher(app);
            //SetupHardwareTimer(app);
            //SetupTimerService(app);
        }

        //private void SetupHardwareTimer(IApplicationBuilder app)
        //{
        //    var timerTimer = new System.Timers.Timer(200);
        //    timerTimer.Elapsed += (x, y) => { app.ApplicationServices.GetService<EventQueue>()!.HandleEvent(new TimerEvent(y.SignalTime)); };
        //    timerTimer.AutoReset = true;
        //    timerTimer.Enabled = true;
        //}

        //private void SetupTimerService(IApplicationBuilder app)
        //{
        //    var timerService = app.ApplicationServices.GetService<EventTimerService>()!;
        //    timerService.StartTimer(
        //        DateTime.Now, TimeSpan.FromSeconds(1), 
        //            () => app.ApplicationServices.GetService<EventQueue>()!.HandleEvent(new ReadTemperatureEvent())
        //        );    
        //}

        //private void SetupDispatcher(IApplicationBuilder app)
        //{
        //    var controller = app.ApplicationServices.GetService<TemperatureController>()!;
        //    var heater = app.ApplicationServices.GetService<Heater>()!;
        //    var dispatcher = app.ApplicationServices.GetService<Dispatcher>()!;
        //    var timerService = app.ApplicationServices.GetService<EventTimerService>()!;



        //    dispatcher.Subscribe<HeaterOffEvent>(heater);
        //    dispatcher.Subscribe<HeaterOnEvent>(heater);
        //    dispatcher.Subscribe<TemperatureEvent>(controller);
        //    dispatcher.Subscribe<TemperatureControllerOffEvent>(controller);
        //    dispatcher.Subscribe<TemperatureControllerOnEvent>(controller);
        //    dispatcher.Subscribe(timerService);

        //    app.ApplicationServices.GetService<Dispatcher>()!.Subscribe(
        //        new TemperatureSensor(
        //            app.ApplicationServices.GetService<EventQueue>()!,
        //            app.ApplicationServices.GetService<ISensorHAL>()!
        //            )
        //        );
        //}
    }
}
