﻿namespace HeatingAPI.Models
{
    public class TemperatureDTO
    {
        public double Celsius { get; set; }

        public TemperatureDTO() { }
    }
}
