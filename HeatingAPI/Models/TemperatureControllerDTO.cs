﻿using Controller;
using RuntimeLib;

namespace HeatingAPI.Models
{
    public class TemperatureControllerDTO
    {
        public double? Low { get; set; }
        public double? High { get; set; }
        public string State { get; set; }

        public TemperatureControllerDTO(TemperatureController c)
        {
            Low = c.min?._celsius;
            High = c.max?._celsius;
            State = c.State.ToString();
        }
    }

    public class TemperatureControllerConfig
    {
        public double Low { get; set; }
        public double High { get; set; }
    }
}
