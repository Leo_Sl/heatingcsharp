﻿using RuntimeLib;
using System;

namespace HeatingAPI.Controllers
{
    public class SimulationDTO
    {
        public Temperature Temperature { get; set; }
        public int Id { get; set; }
        public DateTimeOffset Time { get; set; }
    }
}