﻿using Controller;
using HeaterN;
using RuntimeLib;
using Sensor;
using System.Collections.Generic;
using Microsoft.VisualStudio.Threading;
using System.Threading.Tasks;
using System;
using System.Threading;

namespace HeatingAPI
{
    public class EventQueue : ITemperatureSensorCallback, ITemperatureControllerCallback
    {
        private AsyncQueue<IEvent> queue = new AsyncQueue<IEvent>();

        public int Size { get => queue.Count; }
        private readonly HashSet<Type> _acceptedTypes;

        public EventQueue(IEnumerable<Type> acceptedEventTypes)
        {
            _acceptedTypes = new HashSet<Type>(acceptedEventTypes);
        }

        public EventQueue(params Type[] acceptedEventTypes) : this((IEnumerable<Type>)acceptedEventTypes)
        {
        }

        public void Enqueue(IEvent e)
        {
            if (_acceptedTypes.Count > 0 /* ... */  && !_acceptedTypes.Contains(e.GetType()))
            {
                throw new ArgumentException($"Type {e.GetType().FullName} is not an accepted type");
            }
            queue.Enqueue(e);
        }

        public void HandleEvent(TemperatureEvent e)
        {
            queue.Enqueue(e);
        }

        public void HandleEvent(HeaterOffEvent e)
        {
            queue.Enqueue(e);
        }

        public void HandleEvent(HeaterOnEvent e)
        {
            queue.Enqueue(e);
        }

        public void HandleEvent(TemperatureControllerOnEvent e)
        {
            queue.Enqueue(e);
        }

        public void HandleEvent(TemperatureControllerOffEvent e)
        {
            queue.Enqueue(e);
        }

        public void HandleEvent(ReadTemperatureEvent e)
        {
            queue.Enqueue(e);
        }

        public void HandleEvent(TimerEvent e)
        {
            queue.Enqueue(e);
        }

        public Task<IEvent> DequeueAsync()
        {
            return queue.DequeueAsync();
        }

        public Task<IEvent> DequeueAsync(CancellationToken token)
        {
            return queue.DequeueAsync(token);
        }
    }
}
