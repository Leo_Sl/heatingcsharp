﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.Threading;
using RuntimeLib;
using Sensor;
using System.Threading.Tasks;
using System;
using Microsoft.Extensions.DependencyInjection;
using Controller;
using HeatingAPI.Models;
using System.Collections.Generic;
using Microsoft.Extensions.Hosting;
using System.Threading;
using HeatingSimulations;
using HeaterN;
using System.Linq;

namespace HeatingAPI.Controllers
{
    //[Route("Heating")]
    //[ApiController]
    //public class HomeController : ControllerBase
    //{

    //}

    //[Route("Heating/{simulationID}")]
    //[ApiController]
    //public class HeatingController : ControllerBase
    //{
    //    /*
    //     *  Ressources: Simulation @ background     - Controller            - Temperature
    //     *              ^ init with new post/put    put/patch, get          get
    //     */
    //    public HeatingController(int simulationID = -1) // is there an elegant way to prevent something getting injected?
    //    {
    //        Console.WriteLine("Hello");
    //    }


    public class Simulation
    {
        public readonly HeatingSimulation _sim = new HeatingSimulation(Temperature.FromCelsius(22));
        private readonly EventTimerService _timerService = new EventTimerService();
        public readonly EventQueue _eventQueue = new EventQueue();
        public readonly Controller.TemperatureController _controller;
        private readonly TemperatureSensor _sensor;
        public readonly Dispatcher _dispatcher;
        private readonly Heater _heater;
        private readonly System.Timers.Timer _hardwareTimer;
        private Task _task;

        public Simulation()
        {
            // interactive: either event in queue, tick in timer
            // simulation only returns temp if requested from sensor, doesnt update itself outside of requests
            _controller = new Controller.TemperatureController(_eventQueue);
            _sensor = new TemperatureSensor(_eventQueue, _sim);
            _heater = new Heater(_sim);
            _dispatcher = new Dispatcher();

            _dispatcher.Subscribe<HeaterOffEvent>(_heater);
            _dispatcher.Subscribe<HeaterOnEvent>(_heater);
            _dispatcher.Subscribe<TemperatureEvent>(_controller);
            _dispatcher.Subscribe<TemperatureControllerOffEvent>(_controller);
            _dispatcher.Subscribe<TemperatureControllerOnEvent>(_controller);
            _dispatcher.Subscribe(_timerService);
            _dispatcher.Subscribe(_sensor);

            _hardwareTimer = new System.Timers.Timer(200);
            _hardwareTimer.Elapsed += (x, y) => { _eventQueue.HandleEvent(new TimerEvent(y.SignalTime)); };
            _hardwareTimer.AutoReset = true;
            _hardwareTimer.Enabled = true;

            _task = Task.Run(async () =>
            {
                while (true)
                {
                    var ev = await _eventQueue.DequeueAsync();
                    _dispatcher.Publish(ev);
                }
            }
            );
        }
    }

    [Route("Heating")]
    [ApiController]
    public class HeatingController : ControllerBase
    {
        private readonly ISimStorage _storage;
        /*
         *  Ressources: Simulation @ background     - Controller            - Temperature   - Eventqueue - Timerservice
         *              ^ init with new post/put    put/patch, get          get
         *       
         */
        // Should DI request everything ever used, or just some parts & get rest directly?
        //public HeatingController(int simulationID = -1) // is there an elegant way to prevent something getting injected?
        public HeatingController(ISimStorage storage)
        {
            _storage = storage;
            Console.WriteLine("Hello");
        }

        //[HttpPost]
        //public ActionResult PostSim()
        //{
        //    /*
        //     * a simulation is a backgroundservice (or numbers generated on demand...) + controller + get temp?
        //     */

        //    return Accepted();
        //}

        [HttpPost]
        public ActionResult PostSimWithStartTemp(TemperatureDTO? tmpTemp = null)
        {
            var from = tmpTemp is null ? 22.0 : tmpTemp.Celsius;
            var temp = Temperature.FromValue((long)from * 100, 0); // placeholder ...
            Console.WriteLine(temp);
            return Accepted();
        }

        [HttpPut]
        [Route("{simulationID}")]
        public ActionResult PutSim(int simulationID)
        {
            _storage.addSim(simulationID, new Simulation());
            Console.WriteLine("Added new sim @ {0}", simulationID);
            return Accepted();
        }

        static SimulationDTO ToSimDTO(int id, Simulation s)
        {
            return new SimulationDTO
            {
                Id = id,
                Temperature = s._sim.ReadTemperature(),
                Time = s._sim._time.Now
            };
        }

        [HttpGet]
        public ActionResult<IList<SimulationDTO>> GetSimulations()
        {
            var casted = (SimStorage)_storage;
            var sims = from sim in casted._storage select ToSimDTO(sim.Key, sim.Value);
            return sims.ToList();
        }


        [HttpGet]
        [Route("{simulationID}")]
        public ActionResult<SimulationDTO> GetSimulation(int simulationID)
        {
            if (!_storage.Has(simulationID))
            {
                return NotFound();
            }
            return ToSimDTO(simulationID, _storage.Get(simulationID));
        }
        
        [Route("{simulationID}/Temperature")]
        [HttpGet]
        public async Task<ActionResult<TemperatureDTO>> GetTemperatureAsync(int simulationID)
        {
            // is there a way to get things in here, without explicit calls & per DI without class fields...?
            /*
             *  Multiple controllers? how many controllers should exist? 
             */
            // var reader = HttpContext.RequestServices.GetRequiredService<IReader>();
            if (!_storage.Has(simulationID))
            {
                return NotFound();
            }
            var sim = _storage.Get(simulationID);
            var reader = new OneTimeTemperatureReader(sim._dispatcher, sim._eventQueue);
            var temp = await reader.readTempAsync();
            return ToTemperatureDTO(temp);
        }

        [Route("{simulationID}/Controller")]
        [HttpGet]
        public ActionResult<TemperatureControllerDTO> GetControllerSettings(int simulationID)
        {
            if (!_storage.Has(simulationID))
            {
                return NotFound();
            }
            var sim = _storage.Get(simulationID);
            var controller = sim._controller;
            Console.WriteLine(new TemperatureControllerDTO(controller));
            return new TemperatureControllerDTO(controller);
        }

        [Route("{simulationID}/Controller")]
        [HttpPut]
        public ActionResult PostControllerSettings(int simulationID, TemperatureControllerConfig cfg)
        {
            if (!_storage.Has(simulationID))
            {
                return NotFound();
            }
            var sim = _storage.Get(simulationID);
            sim._eventQueue.HandleEvent(new TemperatureControllerOnEvent(Temperature.FromValue((int)(cfg.Low * 100), 0), Temperature.FromValue((int)(cfg.High * 100), 0)));
            // TODO
            return Accepted();
        }

        [Route("{simulationID}/Controller")]
        [HttpPatch]
        public ActionResult PatchControllerSettings(TemperatureControllerConfig cfg)
        {
            return Accepted();
        }

        public static TemperatureDTO ToTemperatureDTO(Temperature temp)
            => new TemperatureDTO { Celsius = Convert.ToDouble(temp.ToString()) };
    }
}
