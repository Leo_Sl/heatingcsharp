﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.VisualStudio;

using RuntimeLib;
using Sensor;
using Microsoft.VisualStudio.Threading;
using System;
using Controller;
using System.IO;
using HeatingSimulations;
using Microsoft.Extensions.DependencyInjection;

namespace HeatingAPI.Controllers
{
    [Route("Temperature")]
    [ApiController]
    public class TemperatureController : ControllerBase
    {
        private readonly EventQueue _queue;
        private readonly Dispatcher _dispatcher;

        public TemperatureController(EventQueue queue, Dispatcher dispatcher)
        {
            _queue = queue;
            _dispatcher = dispatcher;
        }

        class ReadOnce : IEventHandler<TemperatureEvent>
        {
            private AsyncQueue<Temperature> temp = new AsyncQueue<Temperature>();

            public void HandleEvent(TemperatureEvent @event)
            {
                temp.Enqueue(@event.Temperature);
            }

            public Task<Temperature> GetTemperatureAsync()
            {
                return temp!.DequeueAsync();
            }
        }

        [HttpGet]
        public async Task<ActionResult<Temperature>> GetTemperatureAsync()
        {
            var v = HttpContext.RequestServices.GetService<ISensorHAL>();
            ReadOnce temp = new ReadOnce();
            _dispatcher.Subscribe(temp);
            
            _queue.HandleEvent(new ReadTemperatureEvent());
            return await temp.GetTemperatureAsync();
        }

        public class TempPack
        {
            public int low { get; set; }
            public int high { get; set; }
        }

        [HttpPost]
        public ActionResult PostControllerOn(TempPack pack)
        {
            var low = Temperature.FromCelsius(pack.low);
            var high = Temperature.FromCelsius(pack.high);
            _queue.HandleEvent(new TemperatureControllerOnEvent(low, high));
            return Accepted();
        }

        [HttpPost]
        [Route("{t1}/{t2}")]
        public ActionResult PostControllerOn(int t1, int t2)
        {
            _queue.HandleEvent(new TemperatureControllerOnEvent(Temperature.FromCelsius(t1), Temperature.FromCelsius(t2)));
            return Accepted();
        }
    }
}
