﻿using HeaterN;
using Sensor;
using System;
using System.Timers;
using System.Diagnostics;

using RuntimeLib;

namespace HeatingSimulations
{

    public class HeatingSimulation : ISensorHAL, IHeaterHAL
    {
        //public Temperature Temperature { get; private set; }
        private Temperature Temperature { get; set; }
        private readonly DateTimeOffset _start;
        private DateTimeOffset _lastUpdate;
        private bool _heaterOn = false;
        public readonly ITime _time;
        private Temperature _tMin;
        private Temperature _tMax;

        private static Temperature Clamp(Temperature t, Temperature min, Temperature max)
        {
            if (t < min)
            {
                return min;
            }
            if (t > max)
            {
                return max;
            }
            return t;
        }

        public void Update(TimeSpan time)
        {
            _lastUpdate = _time.Now; // thats wrong? time is now at best very weird if mixing both update functions...
            var delta = (int)time.TotalSeconds;
            if (delta < 1)
            {
                delta = 1;
            }
            if (_heaterOn)
            {
                Temperature += Temperature.FromValue(delta, 2);
            }
            else
            {
                Temperature -= Temperature.FromValue(delta, 2);
            }
            Temperature = Clamp(Temperature, Temperature.FromCelsius(-20), Temperature.FromCelsius(60));
        }

        public void Update()
        {
            // this gets called before any heater state switch, so we just update from last update to now
            DateTimeOffset now = _time.Now;
            var delta = (int)(now - _lastUpdate).TotalSeconds;
            _lastUpdate = now;
            if (delta < 1) // still ugly, but if we have subsecond calls, we see no change otherwise (which would be annoying at this point :)
            {
                delta = 1;
            }
            if (_heaterOn)
            {
                Temperature += Temperature.FromValue(delta, 2);
            }
            else
            {
                Temperature -= Temperature.FromValue(delta, 2);
            }
            Temperature = Clamp(Temperature, _tMin, _tMax);
        }

        public HeatingSimulation(ITime time, Temperature startTemp)
        {
            _time = time;
            _start = _time.Now;
            Temperature = startTemp;
            _lastUpdate = _start;
            _tMin = Temperature.FromCelsius(-20);
            _tMax = Temperature.FromCelsius(60);
        }

        //public HeatingSimulation() : this(, Temperature.FromCelsius(22))
        //{

        //}

        private class DefaultTime : ITime {
            public DateTimeOffset Now { get => DateTimeOffset.Now; }
        }

        public HeatingSimulation(Temperature temp) : this(new DefaultTime(), temp)
        {

        }

        public Temperature ReadTemperature()
        {
            Update();
            return Temperature;
        }

        public void SwitchOff()
        {
            Update();
            _heaterOn = false;
        }

        public void SwitchOn()
        {
            Update();
            _heaterOn = true;
        }
    }

}
