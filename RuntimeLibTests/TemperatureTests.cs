using NUnit.Framework;
using RuntimeLib;

namespace RuntimeLibTests
{
    public class TemperatureTests
    {
        Temperature low;
        Temperature high;
        Temperature medium;

        [SetUp]
        public void Setup()
        {
            low = Temperature.FromCelsius(-5);
            medium = Temperature.FromCelsius(15);
            high = Temperature.FromCelsius(30);
        }

        [Test]
        public void TestCompare()
        {
            Assert.Pass();
        }

        [Test]
        public void TestEquals()
        {
            Assert.That(low.Equals(high), Is.False);
            Assert.That(low.Equals(medium), Is.False);
            Assert.That(low.Equals(low), Is.True);
        }

        [Test]
        public void TestOperatorlt()
        {
            Assert.That(low < high);
            Assert.That(medium < high);
            Assert.That(low < medium);
        }
        [Test]
        public void TestOperatorgt()
        {
            var w = high.CompareTo(medium);
            var v = high > medium;
            Assert.That(high > medium);
            Assert.That(medium > low);
            Assert.That(high > low);
        }
        [Test]
        public void TestOperatorEq()
        {
            Temperature eq = low;
            Assert.That(eq == low);
        }

        [Test]
        public void TestOperatorNe()
        {
            Assert.That(low != high);
            Assert.That(medium != low);
        }

        [Test]
        public void TestOperatorLe()
        {
            var low2 = low;
            Assert.That(low2 <= low);
            Assert.That(medium <= high);
        }

        [Test]
        public void TestOperatorGe()
        {
            var low2 = low;
            Assert.That(low >= low2);
            Assert.That(medium >= low);
        }

        [Test]
        public void TestAdd()
        {
            var result = Temperature.FromCelsius(45);
            Assert.That(result == (medium + high), Is.True);
        }

        [Test]
        public void TestSub()
        {
            Assert.That(medium == (high - medium), Is.True);
        }

        [Test]
        public void TestAddDifferentScale()
        {
            var t1 = Temperature.FromValue(2200, 2);
            var t2 = Temperature.FromValue(22, 0);
            var t3 = Temperature.FromValue(220, 1);
            Assert.That(t1 + t2, Is.EqualTo(Temperature.FromValue(44, 0)));
            Assert.That(t2 + t3, Is.EqualTo(Temperature.FromValue(44, 0)));
        }
    }
}