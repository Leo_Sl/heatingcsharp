﻿using RuntimeLib;
using NSubstitute;
using NUnit.Framework;
using System;

namespace TimerServiceTests
{

    public class TimerServiceTests
    {
        private TimerService timerService;
        private Action actionOne;
        private Action actionTwo;
        private Action actionThree;

        [SetUp]
        public void Setup()
        {
            timerService = new TimerService();
            actionOne = Substitute.For<Action>();
            actionTwo = Substitute.For<Action>();
            actionThree = Substitute.For<Action>();
        }

        [Test]
        public void TestEmptyServiceTrigger()
        {
            timerService.Tick(DateTime.Now);
        }

        [Test]
        public void TestAddOneTimer()
        {
            var timer = timerService.StartTimer(DateTime.Now, actionTwo);
            Assert.That(timerService.NumTimers, Is.EqualTo(1));
        }

        [Test]
        public void TestAddThreeTimer()
        {
            DateTime now = DateTime.Now;
            var t1 = timerService.StartTimer(now, actionTwo);
            var t2 = timerService.StartTimer(now + TimeSpan.FromSeconds(10), actionOne);
            var t3 = timerService.StartTimer(now + TimeSpan.FromSeconds(7), actionThree);
            Assert.That(timerService.NumTimers, Is.EqualTo(3));
        }

        [Test]
        public void TestStopOneTimer()
        {
            var timer = timerService.StartTimer(DateTime.Now, actionTwo);
            timerService.StopTimer(timer);
            Assert.That(timerService.NumTimers, Is.EqualTo(0));
        }

        [Test]
        public void TestStopThreeTimers()
        {
            DateTime now = DateTime.Now;
            var t1 = timerService.StartTimer(now, actionTwo);
            var t2 = timerService.StartTimer(now + TimeSpan.FromSeconds(10), actionOne);
            var t3 = timerService.StartTimer(now + TimeSpan.FromSeconds(7), actionThree);
            timerService.StopTimer(t1);
            timerService.StopTimer(t2);
            timerService.StopTimer(t3);
            Assert.That(timerService.NumTimers, Is.EqualTo(0));
        }

        [Test]
        public void TestStopTimerTwice()
        {
            var timer = timerService.StartTimer(DateTime.Now, actionTwo);
            timerService.StopTimer(timer);
            timerService.StopTimer(timer);
            Assert.That(timerService.NumTimers, Is.EqualTo(0));
        }

        [Test]
        public void TestAddThreeTimersAndTestOrder()
        {
            DateTime now = DateTime.Now;
            timerService.StartTimer(now, actionTwo);
            timerService.StartTimer(now + TimeSpan.FromSeconds(10), actionOne);
            timerService.StartTimer(now + TimeSpan.FromSeconds(7), actionThree);
            timerService.Tick(DateTime.Now + TimeSpan.FromSeconds(1000));
            Received.InOrder(() =>
            {
                actionTwo();
                actionThree();
                actionOne();
            });
        }

        [Test]
        public void TestRepeatAfterInterval()
        {
            timerService.StartTimer(DateTime.Now, TimeSpan.FromSeconds(10), actionOne);
            var now = DateTime.Now;
            Func<int, DateTime> time = x => now + TimeSpan.FromSeconds(x);
            timerService.Tick(now);
            timerService.Tick(time(9));
            timerService.Tick(time(10));
            timerService.Tick(time(11));
            timerService.Tick(time(20));
            actionOne.Received(3).Invoke();
        }

        [Test]
        public void TestStartTime()
        {
            DateTime now = DateTime.Now;
            timerService.StartTimer(now, actionOne);
            timerService.StartTimer(now + TimeSpan.FromSeconds(10), actionTwo);
            timerService.Tick(now + TimeSpan.FromSeconds(9));
            actionOne.Received(1);
            timerService.Tick(now + TimeSpan.FromSeconds(10));
            timerService.Tick(now + TimeSpan.FromSeconds(11));
            actionTwo.Received(1);
        }

        [Test]
        public void TestRemoveTimerAfterFiring()
        {
            DateTime now = DateTime.Now;
            var timer = timerService.StartTimer(now, actionOne);
            timerService.Tick(now);
            timerService.StopTimer(timer);
            actionOne.Received(1);
        }

        [Test]
        public void TestRemoveTimerBeforeFiring()
        {
            DateTime now = DateTime.Now;
            var timer = timerService.StartTimer(now, actionOne);
            timerService.StopTimer(timer);
            timerService.Tick(now);
            actionOne.DidNotReceive();
        }

        [Test]
        public void TestRestartOneshotTimer()
        {
            DateTime now = DateTime.Now;
            var timer = timerService.StartTimer(now, actionOne);
            timerService.RestartTimer(timer, now + TimeSpan.FromSeconds(10));
            timerService.Tick(now);
            actionOne.DidNotReceive();
            timerService.Tick(now + TimeSpan.FromSeconds(10));
            actionOne.Received(1);
        }

        [Test]
        public void TestRestartWithoutStart()
        {
            DateTime now = DateTime.Now;
            var timer = timerService.StartTimer(now, actionOne);
            timerService.RestartTimer(timer, now);
            timerService.Tick(now);
            actionOne.Received(1);
        }

        [Test]
        public void TestRestartRepeatingTimer()
        {
            DateTime now = DateTime.Now;
            var timer = timerService.StartTimer(now, actionOne);
            timerService.RestartTimer(timer, now, TimeSpan.FromSeconds(10));
            timerService.Tick(now);
            timerService.Tick(now + TimeSpan.FromSeconds(10));
            actionOne.Received(2);
        }

    }
}