using NUnit.Framework;
using Sensor;
using NSubstitute;

using RuntimeLib;


namespace SensorTest
{
    public class Tests
    {
        TemperatureSensor sensor;
        ITemperatureSensorCallback queueMock;
        ISensorHAL halMock;

        [SetUp]
        public void Setup()
        {
            queueMock = Substitute.For<ITemperatureSensorCallback>();
            halMock = Substitute.For<ISensorHAL>();
            sensor = new TemperatureSensor(queueMock, halMock);
        }


        [TestCase(-8)]
        [TestCase(12)]
        [TestCase(25)]
        public void TestRead(int temp)
        {
            var temperature = Temperature.FromCelsius(temp);
            halMock.ReadTemperature().Returns(temperature);
            sensor.ReadTemperature();
            queueMock.Received().HandleEvent(Arg.Is<TemperatureEvent>(e => e.Temperature == temperature));
        }

        [Test]
        public void TestHandle()
        {
            var temperature = Temperature.FromCelsius(23);
            halMock.ReadTemperature().Returns(temperature);
            sensor.HandleEvent(new ReadTemperatureEvent());
            queueMock.Received().HandleEvent(Arg.Is<TemperatureEvent>(e => e.Temperature == temperature));
        }
    }
}