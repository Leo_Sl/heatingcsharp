using NUnit.Framework;
using HeaterN;
using NSubstitute;


namespace HeaterTest
{

    class DontMindMe : IHeaterHAL
    {
        public void SwitchOff()
        {
            i -= 1;
        }

        public void SwitchOn()
        {
            i += 1;
        }

        public int i;
        
    }

    public class Tests
    {
        private IHeaterHAL HALMock;
        private Heater heater;
        [SetUp]
        public void Setup()
        {
            HALMock = Substitute.For<IHeaterHAL>();
            heater = new Heater(HALMock);
        }

        [Test]
        public void TestTurnHeaterOn()
        {
            HeaterOnEvent e = new HeaterOnEvent();
            heater.HandleEvent(e);
            HALMock.Received().SwitchOn();
        }
        
        [Test]
        public void TestTurnHeaterOffWithoutTurningItOn()
        {
            HeaterOffEvent e = new HeaterOffEvent();
            heater.HandleEvent(e);
            HALMock.DidNotReceive().SwitchOff();
        }

        [Test]
        public void TestTurnHeaterOnThenOff()
        {
            HeaterOnEvent eon = new HeaterOnEvent();
            heater.HandleEvent(eon);
            HALMock.Received().SwitchOn();
            HeaterOffEvent eoff = new HeaterOffEvent();
            heater.HandleEvent(eoff);
            HALMock.Received().SwitchOff();
        }
        
    }
}