using Controller;
using NUnit.Framework;
using NSubstitute;
using HeaterN;
using System;
using Sensor;

using RuntimeLib;

namespace ControllerTest
{
    public class Tests
    {
        TemperatureController controller;
        ITemperatureControllerCallback queueMock;

        [SetUp]
        public void Setup()
        {
            queueMock = Substitute.For<ITemperatureControllerCallback>();
            controller = new TemperatureController(queueMock);
        }

        [Test]
        public void TestOffToOff()
        {
            controller.HandleEvent(new TemperatureControllerOffEvent());
            queueMock.DidNotReceive().HandleEvent(Arg.Any<HeaterOffEvent>());
        }

        [Test]
        public void TestHeaterOffWhenControllerTurnedOff()
        {
            controller.HandleEvent(new TemperatureControllerOnEvent(Temperature.FromCelsius(12), Temperature.FromCelsius(22)));
            controller.HandleEvent(new TemperatureEvent(Temperature.FromCelsius(5)));
            queueMock.Received().HandleEvent(Arg.Any<HeaterOnEvent>());
            controller.HandleEvent(new TemperatureControllerOffEvent());
            queueMock.Received().HandleEvent(Arg.Any<HeaterOffEvent>());
        }

        [Test]
        public void TestHeaterOnlyTurnedOffOnce()
        {
            controller.HandleEvent(new TemperatureControllerOnEvent(Temperature.FromCelsius(12), Temperature.FromCelsius(22)));
            controller.HandleEvent(new TemperatureEvent(Temperature.FromCelsius(5)));
            controller.HandleEvent(new TemperatureEvent(Temperature.FromCelsius(23)));
            controller.HandleEvent(new TemperatureControllerOffEvent());
            queueMock.Received(1).HandleEvent(Arg.Any<HeaterOffEvent>());
        }

        [Test]
        public void TestOffToOn()
        {
            Assert.That(
                () => controller.HandleEvent(new TemperatureControllerOnEvent(Temperature.FromCelsius(32), Temperature.FromCelsius(22)))
                , Throws.TypeOf<ArgumentException>());
        }

        [Test]
        public void TestSwitchHeaterOn()
        {
            controller.HandleEvent(new TemperatureControllerOnEvent(Temperature.FromCelsius(12), Temperature.FromCelsius(22)));
            controller.HandleEvent(new TemperatureEvent(Temperature.FromCelsius(5)));
            queueMock.Received().HandleEvent(Arg.Any<HeaterOnEvent>());
        }

        [Test]
        public void TestSwitchHeaterOff()
        {
            controller.HandleEvent(new TemperatureControllerOnEvent(Temperature.FromCelsius(12), Temperature.FromCelsius(22)));
            controller.HandleEvent(new TemperatureEvent(Temperature.FromCelsius(5)));
            controller.HandleEvent(new TemperatureEvent(Temperature.FromCelsius(23)));
            queueMock.Received().HandleEvent(Arg.Any<HeaterOffEvent>());
        }

        [Test]
        public void TestControllerOnOffOnOff()
        {
            controller.HandleEvent(new TemperatureControllerOnEvent(Temperature.FromCelsius(12), Temperature.FromCelsius(22)));
            controller.HandleEvent(new TemperatureEvent(Temperature.FromCelsius(5)));
            controller.HandleEvent(new TemperatureControllerOffEvent());
            controller.HandleEvent(new TemperatureControllerOnEvent(Temperature.FromCelsius(22), Temperature.FromCelsius(23)));
            controller.HandleEvent(new TemperatureEvent(Temperature.FromCelsius(25)));
            controller.HandleEvent(new TemperatureEvent(Temperature.FromCelsius(15)));
            Received.InOrder(() => {
                queueMock.HandleEvent(Arg.Any<HeaterOnEvent>());
                queueMock.HandleEvent(Arg.Any<HeaterOffEvent>());
                queueMock.HandleEvent(Arg.Any<HeaterOnEvent>());
                });
        }
    }
}