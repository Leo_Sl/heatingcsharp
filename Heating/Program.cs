﻿using System;
using System.Collections.Generic;
using System.Collections;

using HeaterN;
using Controller;
using Sensor;
using HeatingSimulations;
using System.Linq.Expressions;

using RuntimeLib;

namespace Heating
{

    internal class EventQueue : ITemperatureSensorCallback, ITemperatureControllerCallback
    {
        private Queue<IEvent> queue = new Queue<IEvent>(); 

        public int Size { get => queue.Count; }

        public void HandleEvent(TemperatureEvent e)
        {
            queue.Enqueue(e);
        }

        public void HandleEvent(HeaterOffEvent e)
        {
            queue.Enqueue(e);
        }

        public void HandleEvent(HeaterOnEvent e)
        {
            queue.Enqueue(e);
        }

        public void HandleEvent(TemperatureControllerOnEvent e)
        {
            queue.Enqueue(e);
        }

        public void HandleEvent(TemperatureControllerOffEvent e)
        {
            queue.Enqueue(e);
        }

        public void HandleEvent(ReadTemperatureEvent e)
        {
            queue.Enqueue(e);
        }

        public void HandleEvent(TimerEvent e)
        {
            queue.Enqueue(e);
        }

        public IEvent Dequeue()
        {
            return queue.Dequeue();
        }

    }

    internal class SensorReadingPrinter : IEventHandler<TemperatureEvent>
    {
        public void HandleEvent(TemperatureEvent e)
        {
            Console.WriteLine(e.Temperature.ToString());
        }
    }

    internal class ControllerController : IEventHandler<TemperatureEvent>
    {
        EventQueue eq;
        public ControllerController(EventQueue q)
        {
            eq = q;
        }

        public void HandleEvent(TemperatureEvent e)
        {
            if( e.Temperature < Temperature.FromCelsius(0))
            {
                eq.HandleEvent(new TemperatureControllerOnEvent(Temperature.FromCelsius(15), Temperature.FromCelsius(25)));
                Console.WriteLine("Controller ON!");
            }

            if(e.Temperature > Temperature.FromCelsius(40))
            {
                eq.HandleEvent(new TemperatureControllerOnEvent(Temperature.FromCelsius(15), Temperature.FromCelsius(25)));
                Console.WriteLine("Controller ON!");
            }
        }
    }

    class C
    {
        Expression<Func<int>> expr = Expression.Lambda<Func<int>>(Expression.Constant(5));
        public int Val()
        {
            return expr.Compile()();
        }
    }

    internal class Program
    {

        private class Time : ITime
        {
            public DateTimeOffset Now => DateTimeOffset.Now;
        }

        static void Main(string[] args)
        {
            HeatingSimulation simulation = new HeatingSimulation(new Time(), Temperature.FromCelsius(22));
            EventQueue eq = new EventQueue();
            
            var heater = new Heater(simulation);
            var temperatureSensor = new TemperatureSensor(eq, simulation);
            var controller = new TemperatureController(eq);

            EventTimerService timerService = new EventTimerService();

            Dispatcher d = new Dispatcher();
            d.Subscribe<HeaterOffEvent>(heater);
            d.Subscribe<HeaterOnEvent>(heater);
            d.Subscribe<TemperatureEvent>(controller);
            d.Subscribe<TemperatureControllerOffEvent>(controller);
            d.Subscribe<TemperatureControllerOnEvent>(controller);
            d.Subscribe(timerService);
            d.Subscribe(temperatureSensor);

            SensorReadingPrinter srp = new SensorReadingPrinter();
            d.Subscribe<TemperatureEvent>(srp);
            ControllerController cc = new ControllerController(eq);
            d.Subscribe<TemperatureEvent>(cc);

            //timerService.StartTimer(DateTime.Now, TimeSpan.FromSeconds(1), () => simulation.Update(TimeSpan.FromSeconds(10)));
            timerService.StartTimer(DateTime.Now, TimeSpan.FromSeconds(3), () => eq.HandleEvent(new ReadTemperatureEvent()));


            var timerTimer = new System.Timers.Timer(200);
            timerTimer.Elapsed += (x, y) => { eq.HandleEvent(new TimerEvent(y.SignalTime)); };
            timerTimer.AutoReset = true;
            timerTimer.Enabled = true;


            while (true)
            {
                if (eq.Size > 0)
                {
                    //var e2 = eq.Deq();
                    var e2 = eq.Dequeue();
                    //d.Publish(e2);
                    // d.PublishGeneric(e2);
                    //d.Pub(e2);
                    d.Publish(e2);
                    //td.invoke(e2);

                 //   var e = eq.Dequeue();
                //    d.Publish(e);
                }

            }
        }
    }
}
