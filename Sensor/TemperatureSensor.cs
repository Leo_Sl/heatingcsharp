﻿using System;

namespace Sensor
{
    public class TemperatureSensor : ITemperatureSensor
    {
        private readonly ISensorHAL _sensorHAL;
        private readonly ITemperatureSensorCallback _callback;

        public void ReadTemperature()
        {
            var temperature = _sensorHAL.ReadTemperature();
            Console.WriteLine(temperature);
            _callback.HandleEvent(new TemperatureEvent(temperature));
        }

        public TemperatureSensor(ITemperatureSensorCallback eq, ISensorHAL hal)
        {
            _sensorHAL = hal;
            _callback = eq;
        }

        public void HandleEvent(ReadTemperatureEvent e){
            ReadTemperature();
        }
    }
}
