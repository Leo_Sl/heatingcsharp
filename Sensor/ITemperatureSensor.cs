﻿
using RuntimeLib;

namespace Sensor
{
    public interface ITemperatureSensor : IEventHandler<ReadTemperatureEvent>
    {

    }
}
