﻿using RuntimeLib;

namespace Sensor
{
    public interface ISensorHAL
    {
        Temperature ReadTemperature();
    }
}