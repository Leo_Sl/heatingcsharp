﻿
using RuntimeLib;

namespace Sensor
{
    public class TemperatureEvent : IEvent
    {
        public Temperature Temperature { get; private set; }
        public TemperatureEvent(Temperature temp)
        {
            Temperature = temp;
        }
    }
}
