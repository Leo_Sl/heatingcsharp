﻿
using RuntimeLib;

namespace Sensor
{
    public interface ITemperatureSensorCallback : IEventHandler<TemperatureEvent>
    {

    }
}
