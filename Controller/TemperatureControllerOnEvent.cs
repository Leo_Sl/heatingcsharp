﻿using RuntimeLib;

namespace Controller
{
    public class TemperatureControllerOnEvent : IEvent
    {
        public Temperature min { get; private set; }
        public Temperature max { get; private set; }

        public TemperatureControllerOnEvent(Temperature min, Temperature max)
        {
            this.min = min;
            this.max = max;
        }
    }
}