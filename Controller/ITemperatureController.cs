﻿using Sensor;
using RuntimeLib;

namespace Controller
{
    public interface ITemperatureController : IEventHandler<TemperatureEvent>, IEventHandler<TemperatureControllerOffEvent>, IEventHandler<TemperatureControllerOnEvent>
    {

    }
}
