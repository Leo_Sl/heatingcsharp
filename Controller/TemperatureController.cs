﻿using HeaterN;
using Sensor;
using RuntimeLib;
using System;

namespace Controller
{

    public class TemperatureController : ITemperatureController
    {
        public enum TemperatureControllerState
        {
            off,
            on_and_idle,
            on_and_heating
        }

        public TemperatureControllerState State { get; private set; } = TemperatureControllerState.off;
        public Temperature? min { get; private set; }
        public Temperature? max { get; private set; }
        private readonly ITemperatureControllerCallback queue;

        public TemperatureController(ITemperatureControllerCallback queue)
        {
            this.queue = queue;
        }

        public void HandleEvent(TemperatureEvent e)
        {
            switch (State)
            {
                case TemperatureControllerState.off:
                    break;
                case TemperatureControllerState.on_and_idle:
                    if(e.Temperature < min!)
                    {
                        queue.HandleEvent(new HeaterOnEvent());
                        State = TemperatureControllerState.on_and_heating;
                    }
                    break;
                case TemperatureControllerState.on_and_heating:
                    if(e.Temperature > max!)
                    {
                        queue.HandleEvent(new HeaterOffEvent());
                        State = TemperatureControllerState.on_and_idle;
                    }
                    break;
            }
        }

        public void HandleEvent(TemperatureControllerOffEvent e)
        {
            switch (State)
            {
                case TemperatureControllerState.off:
                    break;
                case TemperatureControllerState.on_and_idle:
                    State = TemperatureControllerState.off;
                    break;
                case TemperatureControllerState.on_and_heating:
                    State = TemperatureControllerState.off;
                    queue.HandleEvent(new HeaterOffEvent());
                    break;
            }
        }

        private void AdjustControl(TemperatureControllerOnEvent e)
        {
            if(e.min > e.max)
            {
                throw new ArgumentException("Min is higher than max.");
            }
            min = e.min;
            max = e.max;
        }

        public void HandleEvent(TemperatureControllerOnEvent e)
        {
            switch (State)
            {
                case TemperatureControllerState.off:
                    State = TemperatureControllerState.on_and_idle;
                    goto case TemperatureControllerState.on_and_idle;
                case TemperatureControllerState.on_and_heating:
                case TemperatureControllerState.on_and_idle:
                    AdjustControl(e);
                    break;
            }
        }
    }
}
