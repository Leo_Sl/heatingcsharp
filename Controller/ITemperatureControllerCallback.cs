﻿using HeaterN;

namespace Controller
{
    public interface ITemperatureControllerCallback
    {
        void HandleEvent(HeaterOffEvent e);
        void HandleEvent(HeaterOnEvent e);
    }
}
