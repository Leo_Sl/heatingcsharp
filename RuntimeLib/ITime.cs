﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuntimeLib
{
    public interface ITime
    {
        DateTimeOffset Now { get; }

    }
}
