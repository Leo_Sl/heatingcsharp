﻿using System;
using System.Collections.Generic;

namespace RuntimeLib
{
    public class Dispatcher
    {
        private readonly Dictionary<Type, IEventHandlerList> subscribers = new Dictionary<Type, IEventHandlerList>();


        public void Subscribe<T>(IEventHandler<T> d) where T : IEvent
        {
            if (subscribers.TryGetValue(typeof(T), out var list))
            {
                var evList = (EventHandlerList<T>)list;
                evList.Add(d);
            }
            else
            {
                var evList = new EventHandlerList<T>();
                evList.Add(d);
                subscribers.Add(typeof(T), evList);
            }
        }

        public void Publish(IEvent e)
        {
            if (!subscribers.TryGetValue(e.GetType(), out var list))
            {
                return;
            }

            list.Dispatch(e);
        }
    }
}
