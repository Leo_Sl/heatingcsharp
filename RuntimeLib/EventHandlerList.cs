﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RuntimeLib
{
    class EventHandlerList<TEvent> : IEventHandlerList where TEvent : IEvent
    {
        private readonly List<IEventHandler<TEvent>> _list = new List<IEventHandler<TEvent>>();

        public void Add(IEventHandler<TEvent> action)
        {
            _list.Add(action);
        }

        public void Dispatch(IEvent e)
        {
            var ev = (TEvent)e;
            foreach (var action in _list)
            {
                action.HandleEvent(ev);
            }
        }
    }
}
