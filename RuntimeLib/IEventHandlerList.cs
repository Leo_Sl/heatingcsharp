﻿namespace RuntimeLib
{
    public interface IEventHandlerList
    {

        public void Dispatch(IEvent e);
    }

}