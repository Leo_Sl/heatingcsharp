﻿using RuntimeLib;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace RuntimeLib
{
    public class TimerService : ITimerService
    {
        class Timer : ITimer
        {
            private static int LastId { get; set; } = 0;
            public DateTimeOffset Due { get; set; }

            public TimeSpan Interval { get; set; }
            public Action Action { get; set; }
            public bool IsActive { get; set; }
            public int Id { get; }

            public Timer(DateTimeOffset due, TimeSpan interval, Action action)
            {
                Id = LastId;
                IsActive = true;
                Due = due;
                Interval = interval;
                Action = action;
            }

            public void Fire()
            {
                Action();
            }
        }

        private class TimerComparer : IComparer<Timer>
        {

            public int Compare(Timer? x, Timer? y) => (x, y) switch
            {
                (_, _) when ReferenceEquals(x, y) => 0,
                (null, _) => -1,
                (_, null) => 1,
                (_, _) when x.Due < y.Due => -1,
                (_, _) when y.Due < x.Due => 1,
                (_, _) => x.Id < y.Id ? -1 : 1
            };
        }

        private readonly SortedSet<Timer> _timers = new SortedSet<Timer>(new TimerComparer());
        public int NumTimers { get; private set; }

        public void Tick(DateTimeOffset when)
        {
            for (var current = _timers.FirstOrDefault(); current != null && current.Due <= when; current = _timers.FirstOrDefault())
            {
                current.Fire();
                StopTimer(current);
                _timers.Remove(current);
                if (current.Interval != TimeSpan.Zero)
                {
                    current.Due = when + current.Interval;
                    _timers.Add(current);
                }
            }
        }

        public ITimer StartTimer(DateTimeOffset due, Action action)
        {
            return StartTimer(due, TimeSpan.Zero, action);
        }

        public ITimer StartTimer(DateTimeOffset due, TimeSpan interval, Action action)
        {
            var timer = new Timer(due, interval, action);
            _timers.Add(timer);
            NumTimers++;
            return timer;
        }

        private Timer VerifyTimer(ITimer itimer) 
        {
            if (itimer is Timer timer)
            {
                return timer;
            }
            throw new ArgumentException("Timer has wrong type.");
        }

        private void RemoveTimer(Timer timer)
        {
            if (!timer.IsActive)
            {
                return;
            }
            if (!_timers.Remove(timer))
            {
                throw new ArgumentException("Timer does not belong to this service"); // thats not true - it may just have already stopped...
            }
            NumTimers--;
            timer.IsActive = false;
        }

        public void StopTimer(ITimer itimer)
        {
            var timer = VerifyTimer(itimer);
            RemoveTimer(timer);
        }

        public void RestartTimer(ITimer itimer, DateTimeOffset due, TimeSpan interval)
        {
            var timer = VerifyTimer(itimer);
            RemoveTimer(timer);
            timer.Due = due;
            timer.Interval = interval;
            timer.IsActive = true;
            _timers.Add(timer);
        }

        public void RestartTimer(ITimer timer, DateTimeOffset due)
        {
            RestartTimer(timer, due, TimeSpan.Zero);
        }
    }
}
