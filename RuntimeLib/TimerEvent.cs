﻿using System;

namespace RuntimeLib
{
    public class TimerEvent : IEvent
    {
        public DateTimeOffset Time { get; }

        public TimerEvent(DateTimeOffset time)
        {
            Time = time;
        }
    }
}
