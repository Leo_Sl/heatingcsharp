﻿namespace RuntimeLib
{
    public interface IEventHandler<TEvent> where TEvent : IEvent
    {
        void HandleEvent(TEvent @event);
    }
}