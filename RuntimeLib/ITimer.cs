﻿using System;

namespace RuntimeLib
{
    public interface ITimer
    {
        DateTimeOffset Due { get; }
        TimeSpan Interval { get; }
        bool IsActive { get; }
    }
}