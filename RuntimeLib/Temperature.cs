﻿using System;

namespace RuntimeLib
{
    public readonly struct Temperature
    {
        public readonly int Scale { get; }
        public readonly long _celsius { get; }

        private Temperature(long celsius, int scale) 
        {
            _celsius = celsius;
            Scale = scale;
        }

        public static Temperature FromCelsius(long degreeCelsius)
        {
            return new Temperature(degreeCelsius * 100, 2);
        }

        public static Temperature FromValue(long value, int scale)
        {
            return new Temperature(value, scale);
        }

        public override string ToString()
        {
            return $"{_celsius / (int)Math.Pow(10, Scale) }.{ Math.Abs(_celsius % (int)Math.Pow(10, Scale)) }";
        }

        public override bool Equals(object? obj)
            => obj is Temperature t && normalize(t, this)._celsius == _celsius;

        public override int GetHashCode()
            => HashCode.Combine(Scale, _celsius);

        public int CompareTo(object? obj)
            => obj is Temperature t ? CompareTo(t) : 1;

        public int CompareTo(Temperature t)
        {
            var tSameScale = normalize(t, this);
            return _celsius.CompareTo(tSameScale._celsius);
        }

        private static Temperature normalize(Temperature from, Temperature to)
        {
            return FromValue((int)(from._celsius * Math.Pow(10, to.Scale - from.Scale)), to.Scale);
        }

        public static bool operator <(Temperature a, Temperature b) => a.CompareTo(b) < 0;
        public static bool operator >(Temperature a, Temperature b) => a.CompareTo(b) > 0;
        public static bool operator ==(Temperature a, Temperature b) => a.Equals(b);
        public static bool operator !=(Temperature a, Temperature b) => !a.Equals(b);
        public static bool operator <=(Temperature a, Temperature b) => a.CompareTo(b) <= 0;
        public static bool operator >=(Temperature a, Temperature b) => a.CompareTo(b) >= 0;

        public static Temperature operator +(Temperature a, Temperature b)
        {
            return FromValue(normalize(b, a)._celsius + a._celsius, a.Scale);
        }

        public static Temperature operator -(Temperature a, Temperature b)
        {
            return FromValue(a._celsius - normalize(b, a)._celsius, a.Scale);
        }
    }
}
