﻿namespace RuntimeLib
{
    public class EventTimerService : TimerService, IEventHandler<TimerEvent>
    {
        public void HandleEvent(TimerEvent ev)
        {
            Tick(ev.Time);
        }
    }
}
