﻿using System;

namespace RuntimeLib
{
    public interface ITimerService
    {
        ITimer StartTimer(DateTimeOffset due, Action action);
        ITimer StartTimer(DateTimeOffset due, TimeSpan interval, Action action);
        void StopTimer(ITimer timer);
        void RestartTimer(ITimer timer, DateTimeOffset due, TimeSpan interval);
        void RestartTimer(ITimer timer, DateTimeOffset due);
    }
}