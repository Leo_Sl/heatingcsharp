using HeatingSimulations;
using NUnit.Framework;
using System;

using RuntimeLib;

namespace HeatingSimulationTest
{
    public class Tests
    {
        private HeatingSimulation simulation;

        [SetUp]
        public void Setup()
        {
            simulation = new HeatingSimulation(Temperature.FromCelsius(22));
        }

        [Test]
        public void TestHeaterOn()
        {
            simulation.SwitchOn();
            simulation.Update(TimeSpan.FromSeconds(15));
            Assert.That(simulation.ReadTemperature() > Temperature.FromCelsius(22), Is.True);
        }

        [Test]
        public void TestHeaterOff()
        {
            simulation.SwitchOff();
        //    simulation.Update(TimeSpan.FromSeconds(15));
            Assert.That(simulation.ReadTemperature() < Temperature.FromCelsius(22), Is.True);
        }

        [Test]
        public void TestReadTemperature()
        {
           // Assert.That(simulation.Temperature == simulation.ReadTemperature(), Is.True);
         //   simulation.Update(TimeSpan.FromSeconds(15));
           // Assert.That(simulation.Temperature == simulation.ReadTemperature(), Is.True);
        }
    }
}